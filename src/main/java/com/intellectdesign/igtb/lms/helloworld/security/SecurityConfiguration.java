/*
 * Copyright (c) 2019, Intellect Design Arena Ltd. All rights reserved
 * 
 * These materials are confidential and proprietary to Intellect Design Arena Ltd.
 * and no part of these materials should be reproduced, published, transmitted or
 * distributed in any form or by any means, electronic, mechanical, photocopying,
 * recording or otherwise, or stored in any information storage or retrieval system
 * of any nature nor should the materials be disclosed to third parties or used in
 * any other manner for which this is not authorized, without the prior express
 * written authorization of Intellect Design Arena Ltd.
 *  
 */

package com.intellectdesign.igtb.lms.helloworld.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * 
 * Spring security configuration class.
 * Configure security parameters as per need of microservice.
 * 
 * @author raju.prajapati
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/users").permitAll().antMatchers("/login").permitAll();
	}
}
