/*
 * Copyright (c) 2019, Intellect Design Arena Ltd. All rights reserved
 * 
 * These materials are confidential and proprietary to Intellect Design Arena Ltd.
 * and no part of these materials should be reproduced, published, transmitted or
 * distributed in any form or by any means, electronic, mechanical, photocopying,
 * recording or otherwise, or stored in any information storage or retrieval system
 * of any nature nor should the materials be disclosed to third parties or used in
 * any other manner for which this is not authorized, without the prior express
 * written authorization of Intellect Design Arena Ltd.
 *  
 */

package com.intellectdesign.igtb.lms.helloworld.model;

import java.util.Date;

import javax.validation.constraints.Size;

/**
 * <b>This is sample demo class and needs to be removed. This is just for reference.</b>
 * 
 * Model object to store todo activity.
 * 
 * @author raju.prajapati
 *
 */
public class Todo {
	private int id;
	private String user;
	@Size(min = 10, message = "Enter atleast 10 Characters.")
	private String desc;
	private Date targetDate;
	private boolean isDone;

	public Todo() {
	}

	public Todo(int id, String user, String desc, Date targetDate, boolean isDone) {
		super();
		this.id = id;
		this.user = user;
		this.desc = desc;
		this.targetDate = targetDate;
		this.isDone = isDone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}

	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}

	@Override
	public String toString() {
		return String.format("Todo [id=%s, user=%s, desc=%s, targetDate=%s, isDone=%s]", id, user, desc, targetDate,
				isDone);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Todo other = (Todo) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
