/*
 * Copyright (c) 2019, Intellect Design Arena Ltd. All rights reserved
 * 
 * These materials are confidential and proprietary to Intellect Design Arena Ltd.
 * and no part of these materials should be reproduced, published, transmitted or
 * distributed in any form or by any means, electronic, mechanical, photocopying,
 * recording or otherwise, or stored in any information storage or retrieval system
 * of any nature nor should the materials be disclosed to third parties or used in
 * any other manner for which this is not authorized, without the prior express
 * written authorization of Intellect Design Arena Ltd.
 *  
 */

package com.intellectdesign.igtb.lms.helloworld.repository;

import java.util.List;

import com.intellectdesign.igtb.lms.helloworld.model.Todo;

/**
 * <b>This is sample demo class and needs to be removed. This is just for reference.</b>
 * 
 * Interface for DAO implementation of Todo persistance.
 * @author raju.prajapati
 *
 */
public interface TodoDao {

	/**
	 * fetch todo list from store/db
	 * @param user
	 * @return
	 */
	public List<Todo> retrieveTodos(String user);
	
	/**
	 * add todo activity to store/db
	 * @param todo
	 * @return
	 */
	public boolean add(Todo todo);
	
	/**
	 * remove todo activity from store/db
	 * @param id
	 * @return
	 */
	public boolean delete(int id);
	
	/**
	 * fetch specific todo activity by id
	 * @param id
	 * @return
	 */
	public Todo findById(int id);
	
	/**
	 * fetch full list of todo from store/db
	 * @return
	 */
	public List<Todo> retrieveTodos();
}
