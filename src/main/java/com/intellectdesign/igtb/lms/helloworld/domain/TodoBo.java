/*
 * Copyright (c) 2019, Intellect Design Arena Ltd. All rights reserved
 * 
 * These materials are confidential and proprietary to Intellect Design Arena Ltd.
 * and no part of these materials should be reproduced, published, transmitted or
 * distributed in any form or by any means, electronic, mechanical, photocopying,
 * recording or otherwise, or stored in any information storage or retrieval system
 * of any nature nor should the materials be disclosed to third parties or used in
 * any other manner for which this is not authorized, without the prior express
 * written authorization of Intellect Design Arena Ltd.
 *  
 */

package com.intellectdesign.igtb.lms.helloworld.domain;

import java.util.List;

import org.springframework.stereotype.Component;

import com.intellectdesign.igtb.lms.helloworld.model.Todo;

/**
 * <b>This is sample demo class and needs to be removed. This is just for reference.</b>
 * 
 * Bussiness domain object to implement business logic.
 * @author raju.prajapati
 *
 */
@Component
public class TodoBo {


	/**
	 * method contain logic to find next todo Id.
	 * @param todos
	 * @return
	 */
	public int nextTodoId(List<Todo> todos) {
		int id=0;
		for (Todo todo : todos) {
			if (id==0 || todo.getId()>id)
				id = todo.getId();
		}
		return ++id;
	}
}
