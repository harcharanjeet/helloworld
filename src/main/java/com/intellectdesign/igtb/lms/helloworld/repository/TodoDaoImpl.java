/*
 * Copyright (c) 2019, Intellect Design Arena Ltd. All rights reserved
 * 
 * These materials are confidential and proprietary to Intellect Design Arena Ltd.
 * and no part of these materials should be reproduced, published, transmitted or
 * distributed in any form or by any means, electronic, mechanical, photocopying,
 * recording or otherwise, or stored in any information storage or retrieval system
 * of any nature nor should the materials be disclosed to third parties or used in
 * any other manner for which this is not authorized, without the prior express
 * written authorization of Intellect Design Arena Ltd.
 *  
 */

package com.intellectdesign.igtb.lms.helloworld.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.intellectdesign.igtb.lms.helloworld.model.Todo;

/**
 * <b>This is sample demo class and needs to be removed. This is just for reference.</b>
 * 
 * DAO implementation for todo persistance logic
 * @author raju.prajapati
 *
 */
@Repository
public class TodoDaoImpl implements TodoDao {

	private static List<Todo> todos = new ArrayList<>();
	static {
		todos.add(new Todo(1, "Atul", "Learn Spring MVC", new Date(), false));
		todos.add(new Todo(2, "Atul", "Learn Struts", new Date(), false));
		todos.add(new Todo(3, "Deeksha", "Learn Hibernate", new Date(), false));
	}
	
	@Override
	public List<Todo> retrieveTodos(String user) {
		List<Todo> filteredTodos = new ArrayList<>();
		for (Todo todo : todos) {
			if (todo.getUser().equals(user))
				filteredTodos.add(todo);
		}
		return filteredTodos;
	}

	@Override
	public boolean add(Todo todo) {
		todos.add(todo);
		return true;
	}

	@Override
	public boolean delete(int id) {
		Iterator<Todo> iterator = todos.iterator();
		while (iterator.hasNext()) {
			Todo todo = iterator.next();
			if (todo.getId() == id) {
				iterator.remove();				
			}
		}
		return true;
	}

	@Override
	public Todo findById(int id) {
		for (Todo todo : todos) {
			if (todo.getId() == id)
				return todo;
		}
		return null;
	}

	@Override
	public List<Todo> retrieveTodos() {
		return todos;
	}
	
}
