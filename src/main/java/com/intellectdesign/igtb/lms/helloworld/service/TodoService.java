/*
 * Copyright (c) 2019, Intellect Design Arena Ltd. All rights reserved
 * 
 * These materials are confidential and proprietary to Intellect Design Arena Ltd.
 * and no part of these materials should be reproduced, published, transmitted or
 * distributed in any form or by any means, electronic, mechanical, photocopying,
 * recording or otherwise, or stored in any information storage or retrieval system
 * of any nature nor should the materials be disclosed to third parties or used in
 * any other manner for which this is not authorized, without the prior express
 * written authorization of Intellect Design Arena Ltd.
 *  
 */

package com.intellectdesign.igtb.lms.helloworld.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intellectdesign.igtb.lms.helloworld.domain.TodoBo;
import com.intellectdesign.igtb.lms.helloworld.model.Todo;
import com.intellectdesign.igtb.lms.helloworld.repository.TodoDao;

import lombok.extern.slf4j.Slf4j;

/**
 * <b>This is sample demo class and needs to be removed. This is just for reference.</b>
 * 
 * Service class for Todo which interact with repository and business domain classes to perform business logic.
 * It allows separation between API and repository
 * 
 * @author raju.prajapati
 *
 */
@Slf4j
@Service
public class TodoService {
	
	
	@Autowired
	private TodoBo todoBo;
	
	@Autowired
	private TodoDao todoDao;
	
	/**
	 * Api to fetch todo list of user
	 * @param user user name
	 * @return todo list
	 */
	public List<Todo> retrieveTodos(String user) {
		
		return todoDao.retrieveTodos(user);
	}

	/**
	 * Api to add new todo activity 
	 * @param name user name
	 * @param desc todo description
	 * @param targetDate target completion date
	 * @param isDone todo status
	 * @return new todo activity
	 */
	public Todo addTodo(String name, String desc, Date targetDate, boolean isDone) {
		int todoId  = todoBo.nextTodoId(todoDao.retrieveTodos());
		Todo todo = new Todo(todoId, name, desc, targetDate, isDone);
		todoDao.add(todo);
		return todo;
	}

	/**
	 * Api to remove todo activity by id
	 * @param id
	 */
	public void deleteTodo(int id) {
		todoDao.delete(id);
	}

	/**
	 * Api to fetch todo actitity by id
	 * @param id
	 * @return
	 */
	public Todo retrieveTodo(int id) {
		log.debug("retrieve todo of Id {}", id);
		return todoDao.findById(id);
	}

	/**
	 * Api to update todo activity
	 * @param todo
	 */
	public void updateTodo(Todo todo) {
		boolean retVal = todoDao.delete(todo.getId());
		if (retVal)
			todoDao.add(todo);
	}
}