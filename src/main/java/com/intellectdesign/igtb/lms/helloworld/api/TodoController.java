/*
 * Copyright (c) 2019, Intellect Design Arena Ltd. All rights reserved
 * 
 * These materials are confidential and proprietary to Intellect Design Arena Ltd.
 * and no part of these materials should be reproduced, published, transmitted or
 * distributed in any form or by any means, electronic, mechanical, photocopying,
 * recording or otherwise, or stored in any information storage or retrieval system
 * of any nature nor should the materials be disclosed to third parties or used in
 * any other manner for which this is not authorized, without the prior express
 * written authorization of Intellect Design Arena Ltd.
 *  
 */

package com.intellectdesign.igtb.lms.helloworld.api;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.intellectdesign.igtb.lms.helloworld.model.Todo;
import com.intellectdesign.igtb.lms.helloworld.service.TodoService;

import lombok.extern.slf4j.Slf4j;

/**
 * <b>This is sample demo class and needs to be removed. This is just for reference.</b>
 * 
 * Rest controller class to expose json based rest APIs
 * @author raju.prajapati
 *
 */

@Slf4j
@RestController
public class TodoController {
	@Autowired
	private TodoService todoService;
	
	/**
	 * Rest endpoint to provide todo list of specific user.
	 * @param name user name
	 * @return todo list
	 */
	@GetMapping("/users/{name}/todos")
	public List<Todo> retrieveTodos(@PathVariable String name) {
		log.debug("Retrieve todo list of {}", name);
		return todoService.retrieveTodos(name);
	}

	/**
	 * Rest endpoint to provide todo item of specific user and id.
	 * @param name user name
	 * @param id todo id
	 * @return todo item detail
	 * @throws Exception
	 */
	@GetMapping(path="/users/{name}/todos/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Resource<Todo> retrieveTodo(@PathVariable String name, @PathVariable int id) throws Exception {
		Todo todo = todoService.retrieveTodo(id);
		if (todo == null)
			throw new Exception("Something"); // this is sample. Ideally custom Exception should be thrown instead of Exception. 
		Resource<Todo> todoResource = new Resource<>(todo);
		return todoResource;
	}

	/**
	 * Rest endpoint to add or update todo item for user.
	 * @param name user name
	 * @param todo todo item detail
	 * @return todo item added/updated
	 */
	@PostMapping("/users/{name}/todos")
	public ResponseEntity<?> add(@PathVariable String name, @Valid @RequestBody Todo todo) {
		Todo createdTodo = todoService.addTodo(name, todo.getDesc(), todo.getTargetDate(), todo.isDone());
		if (createdTodo == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(createdTodo.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
}